val Http4sVersion = "0.23.10"
val CirceVersion = "0.14.1"
val LogbackVersion = "1.2.10"
val reactiveMongo = "1.0.10"
val sendGrid = "4.8.3"
val bcrypt = "0.9.0"
val jwtScala = "9.0.4"
val scalaTest = "3.2.11"
val scalaMock = "5.2.0"
val pureConfig = "0.17.1"

lazy val root = (project in file("."))
  .settings(
    organization := "com.example",
    name := "ScalaTest",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.13.8",
    libraryDependencies ++= Seq(
      "org.http4s"      %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s"      %% "http4s-blaze-client" % Http4sVersion,
      "org.http4s"      %% "http4s-circe"        % Http4sVersion,
      "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
      "io.circe"        %% "circe-generic"       % CirceVersion,
      "ch.qos.logback"  %  "logback-classic"     % LogbackVersion % Runtime,
      "org.scalameta"   %% "svm-subs"            % "20.2.0",
      "org.reactivemongo" %% "reactivemongo"     % reactiveMongo,
      "com.sendgrid"    % "sendgrid-java"        % sendGrid,
      "at.favre.lib"    % "bcrypt"               % bcrypt,
      "com.github.jwt-scala" %% "jwt-circe"      % jwtScala,
      "org.scalatest"   %% "scalatest"           % scalaTest,
      "org.scalamock"   %% "scalamock"           % scalaMock,
      "org.typelevel"   %% "log4cats-slf4j"      % "2.2.0",
      "com.github.pureconfig" %% "pureconfig"    % pureConfig,
      "com.github.pureconfig" %% "pureconfig-cats-effect" % pureConfig,
      "com.github.pureconfig" %% "pureconfig-squants" % pureConfig,
    )
  )
