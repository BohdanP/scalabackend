package com.example.scalatest.dao.reactiveMongo

import com.example.scalatest.config.ApplicationConfig
import com.example.scalatest.models.Item
import reactivemongo.api.MongoConnection

import scala.concurrent.ExecutionContext

class ItemsDAOImpl(config: ApplicationConfig, connection: MongoConnection)(implicit ex: ExecutionContext)
  extends ReactiveMongoDAOImpl[Item](config.mongo.dbName, "items", connection){

}
