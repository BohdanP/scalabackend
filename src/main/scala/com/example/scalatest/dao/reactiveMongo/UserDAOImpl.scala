package com.example.scalatest
package dao.reactiveMongo

import cats.effect.IO
import config.ApplicationConfig
import models.User
import dao.UserDAO

import reactivemongo.api.bson._


import reactivemongo.api.MongoConnection
import scala.concurrent.ExecutionContext

class UserDAOImpl (config: ApplicationConfig, connection: MongoConnection)(implicit ex: ExecutionContext)
  extends ReactiveMongoDAOImpl[User](config.mongo.dbName, "users", connection)
    with UserDAO{
      override def getByEmail(email: String): IO[Option[User]] = withCollection {
        _.find(BSONDocument("email" -> email)).one[User]
      }
}
