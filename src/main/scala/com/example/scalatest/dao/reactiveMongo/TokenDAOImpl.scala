package com.example.scalatest
package dao
package reactiveMongo

import config.ApplicationConfig
import models.Token

import cats.effect.IO
import reactivemongo.api.MongoConnection
import reactivemongo.api.bson.BSONDocument

import scala.concurrent.ExecutionContext

class TokenDAOImpl(config: ApplicationConfig, connection: MongoConnection)(implicit ex: ExecutionContext)
  extends ReactiveMongoDAOImpl[Token](config.mongo.dbName, "token", connection)
    with TokenDAO {
  override def getByBody(body: String): IO[Option[Token]] = withCollection {
    _.find(BSONDocument("body" -> body)).one[Token]
  }

  override def getByUserId(userId: String): IO[Option[Token]] = withCollection {
    _.find(BSONDocument("userId" -> userId)).one[Token]
  }
}
