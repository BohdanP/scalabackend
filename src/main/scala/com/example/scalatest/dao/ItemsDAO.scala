package com.example.scalatest.dao

import com.example.scalatest.models.Item

trait ItemsDAO extends DAO[String, Item] {

}
