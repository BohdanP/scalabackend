package com.example.scalatest
package dao

import cats.effect.IO
import models.HasId
trait DAO[ID, M <: HasId[ID]] {
  def getById(id: ID): IO[Option[M]]

  def getAll: IO[Seq[M]]

  def createOne(model: M): IO[ID]

  def createMany(models: Seq[M]): IO[Seq[ID]]

  def update(model: M): IO[Boolean]

  def delete(id: ID): IO[Boolean]

  def deleteMany(ids: Seq[String]): IO[Boolean]

}
