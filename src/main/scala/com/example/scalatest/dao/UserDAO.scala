package com.example.scalatest
package dao

import cats.effect.IO
import models.User

trait UserDAO extends DAO[String, User]{
  def getByEmail(email: String): IO[Option[User]]
}
