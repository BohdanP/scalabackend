package com.example.scalatest
package dao

import config.ApplicationConfig
import dao.reactiveMongo._
import reactivemongo.api.MongoConnection

import cats.effect.IO
import scala.concurrent.ExecutionContext

final case class DAOs(userDAO: UserDAO,
                      tokenDAO: TokenDAO,
                     )
object DAOs {
  def create(config: ApplicationConfig,
             connection: MongoConnection)(implicit ex: ExecutionContext): IO[DAOs] = {
    IO.pure(DAOs(
      new UserDAOImpl(config, connection),
      new TokenDAOImpl(config, connection),
    ))
  }
}
