package com.example.scalatest
package config

final case class JwtConfig(ttl: Long, secret: String)
