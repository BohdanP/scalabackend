package com.example.scalatest.config

final case class TokenConfig(emailTtl: Long,invitationTtl: Long, otpTtl: Long)