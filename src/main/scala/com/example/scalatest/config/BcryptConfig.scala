package com.example.scalatest
package config

final case class BcryptConfig(rounds: Int)
