package com.example.scalatest
package config

final case class MongoConfig(uri: String, dbName: String)
