package com.example.scalatest
package config

final case class HttpConfig(host: String, port: Int)

