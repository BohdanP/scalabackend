package com.example.scalatest.config

final case class EmailConfig(emailFrom: String, sendGridApiKey: String)
