package com.example.scalatest
package Controllers

import com.example.scalatest.services.Services
import cats.effect.IO
import com.example.scalatest.Controllers.impl.{AuthController, ItemsController}
import com.example.scalatest.services
import services.helpers.JwtHelper
import org.http4s.{EntityDecoder, HttpApp, HttpRoutes, Request, Response}
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import org.http4s.server.middleware._
import org.http4s.server.middleware.RequestLogger
import exceptions.Exceptions._

import scala.concurrent.duration.DurationInt

trait Controller {
  val dsl: Http4sDsl[IO] = Http4sDsl[IO]
  import dsl._

  object TokenParam extends QueryParamDecoderMatcher[String]("token")

  def routes: HttpRoutes[IO]

  def parseAndHandleError[M](req: Request[IO])(io: M => IO[Response[IO]])(implicit decoder: EntityDecoder[IO, M]): IO[Response[IO]] = for {
    body <- req.as[M]
    resp <- io(body)
  } yield resp

  def errorHandler(io: IO[Response[IO]]): IO[Response[IO]] = io
    .onError(e => IO(e.printStackTrace()))
    .handleErrorWith {
      case ForbiddenException(msg) => Forbidden(msg)
      case NotFoundException(msg) => NotFound(msg)
      case WrongCredentialsException => Forbidden(WrongCredentialsException.getMessage)
      case e: ReportTemplateUnknownException => NotFound(e.getMessage)
      case TooManyRequestsException(msg) => TooManyRequests(msg)
      case InvalidTokenException => NotFound(InvalidTokenException.getMessage)
      case InvitationExpiredException => Forbidden(InvitationExpiredException.getMessage)
      case WrongJsonException(msg) => BadRequest(msg)
      case EmailNotConfirmedException => Forbidden(EmailNotConfirmedException.getMessage)
      case IntegrationAlreadyExistsException => Conflict(IntegrationAlreadyExistsException.getMessage)
      case other =>
        other.printStackTrace()
        InternalServerError()
    }

}

object Controller {
  val dsl: Http4sDsl[IO] = Http4sDsl[IO]

  def apply(services: Services): HttpApp[IO] = {
    val middleware: HttpRoutes[IO] => HttpRoutes[IO] =
      ((http: HttpRoutes[IO]) => AutoSlash(http))
        .andThen(http => CORS.policy.httpRoutes(http))
        .andThen(http => Timeout(30.seconds)(http))

    implicit val jwtHelper: JwtHelper = services.jwtHelper

    val loggers: HttpApp[IO] => HttpApp[IO] = {
      { http: HttpApp[IO] =>
        RequestLogger.httpApp(logHeaders = false, logBody = false)(http)
      }.andThen { http: HttpApp[IO] =>
        ResponseLogger.httpApp(logHeaders = false, logBody = false)(http)
      }
    }

    val itemsController = new ItemsController(services.itemsService)
    val authController = new AuthController(services.tokenService, services.authService)

    val routes = Router("api/v1" -> Router(
      "items" -> itemsController.routes,
      "auth" -> authController.routes
    ))

    loggers(middleware(routes).orNotFound)
  }
}
