package com.example.scalatest.Controllers.impl

import cats.effect.IO
import com.example.scalatest.services.main.ItemsService
import com.example.scalatest.Controllers.Controller
import com.example.scalatest.models.Item
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.circe.CirceEntityEncoder._
import io.circe.generic.auto._

import java.util.UUID

class ItemsController(itemsService: ItemsService) extends Controller{
  import dsl._

  override def routes: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root => itemsService.getAllItems().flatMap(Ok(_))
    case GET -> Root / id => itemsService.getItem(UUID.fromString(id)).flatMap(Ok(_))
    case req@POST -> Root => parseAndHandleError[Item](req){ item =>
      itemsService.createItem(item).flatMap(Ok(_))
    }
    case req@PUT -> Root => parseAndHandleError[Item](req){ item =>
      itemsService.updateItem(item).flatMap(Ok(_))
    }
    case DELETE -> Root / id => itemsService.deleteItem(UUID.fromString(id)).flatMap(Ok(_))
  }

}
