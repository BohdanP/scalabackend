package com.example.scalatest
package Controllers
package impl

import cats.effect.{GenTemporal, IO}
import org.http4s.HttpRoutes
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.circe.CirceEntityEncoder._

import services.main.{AuthService, TokenService}
import models.dto._
import services.helpers.JwtHelper
import io.circe.generic.auto._

class AuthController(tokenService: TokenService, authService: AuthService)
                    (implicit jwtHelper: JwtHelper) extends Controller{
  import dsl._
  override def routes: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / "email" / "confirm" / "check" :? TokenParam(token) => errorHandler {
      tokenService
        .existsToken(token)
        .flatMap(Ok(_))
    }

    case req@PUT -> Root / "email" / "confirm" => parseAndHandleError[ConfirmEmailDTO](req) { confirmEmail =>
      authService
        .confirmEmail(confirmEmail.token)
        .flatMap(Ok(_))
    }
    case req@POST -> Root / "signUp" => parseAndHandleError[CredentialsDTO](req) { credentials =>
      authService
        .createUser(credentials.email, credentials.password)
        .flatMap(Ok(_))

    }
    case req@POST -> Root / "signIn" => parseAndHandleError[CredentialsDTO](req) { credentials =>
      authService
        .signIn(credentials.email, credentials.password)
        .flatMap(Ok(_))
    }
  }
}
