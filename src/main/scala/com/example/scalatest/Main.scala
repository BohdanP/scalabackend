package com.example.scalatest

import cats.effect.{ExitCode, IO, IOApp, Resource}
import cats.implicits.catsSyntaxApplicativeId
import org.typelevel.log4cats.slf4j.Slf4jLogger
import services.Services
import Controllers.Controller
import config.ApplicationConfig
import dao.reactiveMongo.ReactiveMongoDAOImpl
import dao.DAOs

import scala.concurrent.ExecutionContext

object Main extends IOApp{
  override def run(args: List[String]): IO[ExitCode] = {
    val appConfig = ApplicationConfig.load()
    import ExecutionContext.Implicits.global

    val resources = for {
      log <- Resource.eval(Slf4jLogger.fromClass[IO](getClass))
      _ <- Resource.eval(log.info("Initializing Db"))
      connection <- Resource.eval(ReactiveMongoDAOImpl.getConnection(appConfig.mongo.uri))
    } yield (log, connection)

    resources.use {
      case (log, connection) =>
        for {
          _ <- log.info("Initializing services")
          daos <- DAOs.create(appConfig, connection)
          services <- Services.create(appConfig, daos)
          routes <- Controller(services).pure[IO]
          _ <- log.info("Starting Http server")
          exitCode <- HttpServer.makeAndStart(appConfig.http, routes)
        } yield exitCode
    }


  }
}
