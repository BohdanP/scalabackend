package com.example.scalatest.exceptions

object Exceptions {
  class AppException(msg: String) extends RuntimeException(msg)
  case class NotFoundException(text: String) extends AppException(text)
  case class ForbiddenException(text: String) extends AppException(text)
  case class ReportTemplateUnknownException(reportName: String) extends AppException(s"Such report does not exist: $reportName")
  case object WrongFileTypePassedException extends AppException("File type query parameter can be CSV, PDF, XLSX")
  case class MongoException(code: Int) extends AppException(s"mongo exception. Code: $code")
  case object WrongCredentialsException extends AppException("Wrong credentials")
  case object UnauthorizedException extends AppException("Unauthorized")
  case class TooManyRequestsException(text: String) extends AppException(text)
  case object InvalidTokenException extends AppException("Invalid token")
  case object InvitationExpiredException extends AppException("Invitation expired")
  case class WrongJsonException(text: String) extends AppException(text)
  case object EmailNotConfirmedException extends AppException("Email is not confirmed")
  case object IntegrationAlreadyExistsException extends AppException("Integration already exists")
}
