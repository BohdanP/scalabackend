package com.example.scalatest
package services

import config.ApplicationConfig
import cats.effect.IO
import services.helpers._
import services.helpers.impl.UUIDHelperImpl
import dao.DAOs
import services.helpers.impl._
import services.main._
import services.main.impl._

import java.time

final case class Services(itemsService: ItemsService,
                          authService: AuthService,
                          tokenService: TokenService,
                          userService: UserService,
                          emailService: EmailService,
                          bcryptHelper: BCryptHelper,
                          jwtHelper: JwtHelper
                         )

object Services {
  def create(appConfig: ApplicationConfig, daos: DAOs): IO[Services] = {
    implicit val clock: time.Clock = java.time.Clock.systemUTC()
    implicit val uuid: UUIDHelperImpl = new UUIDHelperImpl

    val emailService = new EmailServiceImpl(appConfig.email)
    val userService = new UserServiceImpl(daos.userDAO)
    val tokenService = new TokenServiceImpl(daos.tokenDAO, appConfig.token)
    val bcryptHelper = new BCryptHelperImpl(appConfig.bcrypt)
    val jwtHelper = new JwtHelperImpl(appConfig.jwt)

    val authService = new AuthServiceImpl(
      emailService,
      tokenService,
      bcryptHelper,
      userService,
      jwtHelper,
    )

    IO.pure(Services(
      new ItemsServiceImpl(),
      authService = authService,
      tokenService = tokenService,
      userService = userService,
      emailService = emailService,
      bcryptHelper = bcryptHelper,
      jwtHelper = jwtHelper,
    ))
  }
}
