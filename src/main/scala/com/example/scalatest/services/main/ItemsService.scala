package com.example.scalatest.services.main

import cats.effect.IO
import com.example.scalatest.models.Item

import java.util.UUID

trait ItemsService {
  def createItem(item: Item): IO[String]
  def getItem(id: UUID): IO[Option[Item]]
  def getAllItems(): IO[Seq[Item]]
  def updateItem(item: Item): IO[Boolean]
  def deleteItem(id: UUID): IO[Boolean]
}
