package com.example.scalatest.services.main.impl

import cats.effect.IO
import com.example.scalatest.dao.ItemsDAO
import com.example.scalatest.models.Item
import com.example.scalatest.services.helpers.UUIDHelper
import com.example.scalatest.services.main._

import java.util.UUID

class ItemsServiceImpl(itemsDAO: ItemsDAO)(implicit uuid: UUIDHelper) extends ItemsService{
  override def createItem(itemBody: Item): IO[String] = {
    val item = Item(
      id = uuid.generate,
      name = itemBody.name
    )
    itemsDAO.createOne(item)
  }

  override def getItem(id: UUID): IO[Option[Item]] = {
    itemsDAO.getById(id.toString)
  }

  override def getAllItems(): IO[Seq[Item]] = itemsDAO.getAll

  override def updateItem(item: Item): IO[Boolean] = {
    itemsDAO.update(item)
  }

  override def deleteItem(id: UUID): IO[Boolean] = {
    itemsDAO.delete(id.toString)
  }
}

