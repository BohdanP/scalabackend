package com.example.scalatest
package services.main.impl

import cats.effect.IO
import config.EmailConfig
import services.main.EmailService
import com.sendgrid.helpers.mail.Mail
import com.sendgrid.helpers.mail.objects.{Attachments, Content, Email}
import com.sendgrid.{Method, Request, SendGrid}

class EmailServiceImpl(emailConfig: EmailConfig) extends EmailService {

  override def sendEmail(title: String,
                         emailTo: String,
                         contentHtml: String): IO[Unit] = IO {
    val sendGrid = new SendGrid(emailConfig.sendGridApiKey)
    val mail = new Mail(
      new Email(emailConfig.emailFrom),
      title,
      new Email(emailTo),
      new Content("text/html", contentHtml)
    )

    val request = new Request()
    request.setMethod(Method.POST)
    request.setEndpoint("mail/send")
    request.setBody(mail.build())
    sendGrid.api(request)
  }.flatMap { response =>
    response.getStatusCode match {
      case 200 | 202 => IO(println("Email sent successfully"))
      case code => IO.raiseError(new Throwable(s"Email was not sent,code: $code"))
    }
  }
}