package com.example.scalatest
package services.main
package impl

import cats.effect.IO
import models.dto.AuthResponse
import exceptions.Exceptions._
import services.helpers.{BCryptHelper, JwtHelper}

import java.time.{Clock, Instant}

class AuthServiceImpl(emailService: EmailService,
                      tokenService: TokenService,
                      bCryptHelper: BCryptHelper,
                      userService: UserService,
                      jwtHelper: JwtHelper)(implicit clock: Clock) extends AuthService {
  override def createUser(email: String, password: String): IO[Unit] =
    for {
      userId <- userService.createUser(
        email,
        password = bCryptHelper.encrypt(password)
      )
      token <- tokenService.createToken(userId)
      _ <- emailService.sendEmail("Confirmation", email, token.body)
    } yield ()

  override def confirmEmail(tokenString: String): IO[Unit] =
    for {
      token <- tokenService.getByBody(tokenString)
        .handleErrorWith {
          case _: NotFoundException => IO.raiseError(InvalidTokenException)
        }
      user <- if (!tokenService.isTokenExpired(token)) {
        userService.getById(token.userId)
      } else {
        IO.raiseError(InvalidTokenException)
      }
      _ <- userService.update(user.copy(
        updatedAt = Instant.now(clock),
        accountConfirmed = true
      ))
    } yield ()

  override def signIn(email: String, password: String): IO[AuthResponse] =
    for {
      user <- userService.getByEmail(email)
      .handleErrorWith {
      case _: NotFoundException => IO.raiseError(WrongCredentialsException)
      }
      jwt <- user match {
        case user if !user.accountConfirmed =>
          IO.raiseError(EmailNotConfirmedException)
        case user if bCryptHelper.compare(password, user.password) =>
          IO.pure(jwtHelper.encode(user))
        case _ => IO.raiseError(WrongCredentialsException)
      }
    } yield AuthResponse(jwt, user.id, user.email)
}
