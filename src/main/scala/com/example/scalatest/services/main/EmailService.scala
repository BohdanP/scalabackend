package com.example.scalatest.services.main

import cats.effect.IO

trait EmailService {
  def sendEmail(title: String, emailTo: String, contentHtml: String): IO[Unit]
}
