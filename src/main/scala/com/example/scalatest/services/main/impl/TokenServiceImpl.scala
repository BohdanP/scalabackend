package com.example.scalatest
package services.main
package impl

import cats.effect.IO
import config.TokenConfig
import dao.TokenDAO
import exceptions.Exceptions._
import models._
import services.helpers.UUIDHelper

import java.time.{Clock, Instant}
import java.time.temporal.ChronoUnit

class TokenServiceImpl(tokenDao: TokenDAO, tokenConfig: TokenConfig)(implicit clock: Clock,
                                           uuid: UUIDHelper) extends TokenService {
  override def createToken(userId: String): IO[Token] =
  {
    val body = uuid.generate
    val token = Token(
      uuid.generate,
      userId,
      body,
      Instant.now(clock),
    )
    tokenDao.getByUserId(userId)
      .flatMap {
        case Some(tkn) => tokenDao.update(token.copy(id = tkn.id))
          .flatMap(_ => IO.pure(token))
        case None => tokenDao.createOne(token)
          .flatMap(_ => IO.pure(token))
      }
  }

  override def existsToken(body: String): IO[Boolean] =
    tokenDao.getByBody(body).flatMap {
      case Some(token) => IO.pure(true)
      case _ => IO.pure(false)
    }

  override def isTokenExpired(token: Token): Boolean =
    token.timestamp.plus(tokenConfig.emailTtl, ChronoUnit.DAYS).isBefore(Instant.now(clock))

  override def getByBody(body: String): IO[Token] =
    tokenDao.getByBody(body).flatMap {
      case Some(token) => IO.pure(token)
      case None => IO.raiseError(NotFoundException("Token not found"))
    }
}
