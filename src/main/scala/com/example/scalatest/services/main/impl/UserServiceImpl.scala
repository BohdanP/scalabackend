package com.example.scalatest
package services.main
package impl

import exceptions.Exceptions._
import services.helpers.UUIDHelper
import cats.effect.IO
import com.example.scalatest.dao.UserDAO
import models.User

import java.time.{Clock, Instant}

class UserServiceImpl(userDAO: UserDAO)(implicit clock: Clock,
                                        uuid: UUIDHelper) extends UserService {
  override def createUser(email: String, password: String): IO[String] = {
    val now = Instant.now(clock)
    val user = User(
      id = uuid.generate,
      email = email,
      password = password,
      createdAt = now,
      updatedAt = now,
      accountConfirmed = false,
    )
    createUserEmailNotTaken(user, email)
  }

  def createUserEmailNotTaken(user: User, email: String): IO[String] =
    userDAO.getByEmail(email).flatMap {
      case Some(usr) if usr.accountConfirmed =>
        IO.raiseError(ForbiddenException("email is already taken"))
      case Some(usr) =>
        userDAO.update(user.copy(id = usr.id)).map(_ => usr.id)
      case _ =>
        userDAO.createOne(user)
    }

  override def getById(id: String): IO[User] =
    userDAO.getById(id).flatMap(getOrThrow)

  override def getByEmail(email: String): IO[User] =
    userDAO.getByEmail(email).flatMap(getOrThrow)

  override def update(user: User): IO[Boolean] =
    userDAO.update(user)

  def getOrThrow(maybeUser: Option[User]): IO[User] =
    maybeUser match {
      case Some(user) => IO.pure(user)
      case None => IO.raiseError(NotFoundException("User not found"))
    }
}
