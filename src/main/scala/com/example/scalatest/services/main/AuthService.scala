package com.example.scalatest
package services.main

import cats.effect.IO
import models.dto.AuthResponse

trait AuthService {
  def createUser(email: String, password: String): IO[Unit]

  def confirmEmail(token: String): IO[Unit]

  def signIn(email: String, password: String): IO[AuthResponse]
}
