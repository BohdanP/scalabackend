package com.example.scalatest
package services.main

import cats.effect.IO
import models.User

trait UserService {
  def getById(id: String): IO[User]

  def getByEmail(email: String): IO[User]

  def update(user: User): IO[Boolean]

  def createUser(email: String, password: String): IO[String]
}
