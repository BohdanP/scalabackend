package com.example.scalatest
package services.main

import models.Token
import cats.effect.IO

trait TokenService {

  def createToken(userId: String): IO[Token]

  def getByBody(body: String): IO[Token]

  def existsToken(body: String): IO[Boolean]

  def isTokenExpired(token: Token): Boolean
}

