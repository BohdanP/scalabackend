package com.example.scalatest.services.helpers

trait UUIDHelper {
  def generate: String
}
