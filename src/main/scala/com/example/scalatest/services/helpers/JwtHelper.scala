package com.example.scalatest.services.helpers

import com.example.scalatest.models.User

import scala.util.Try

trait JwtHelper {
  def encode(user: User, ttl: Option[Long] = None): String

  def decode(token: String): Try[String]
}
