package com.example.scalatest
package services.helpers
package impl

import java.util.UUID

class UUIDHelperImpl extends UUIDHelper {
  override def generate: String = UUID.randomUUID().toString
}
