package com.example.scalatest.models

import java.time.Instant
import reactivemongo.api.bson.Macros.Annotations.Key
import reactivemongo.api.bson.{BSONDocumentHandler, Macros}

final case class User(@Key("_id") id: String,
                      email: String,
                      password: String,
                      accountConfirmed: Boolean,
                      createdAt: Instant,
                      updatedAt: Instant,
                     ) extends HasId[String]

object User {
  implicit val handler: BSONDocumentHandler[User] = Macros.handler[User]
}
