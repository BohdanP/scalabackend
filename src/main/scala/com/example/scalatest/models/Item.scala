package com.example.scalatest.models

import reactivemongo.api.bson.{BSONDocumentHandler, Macros}
import reactivemongo.api.bson.Macros.Annotations.Key

final case class Item(@Key("_id") id: String, name: String) extends HasId[String]

object Item {
  implicit val handler: BSONDocumentHandler[Item] = Macros.handler[Item]
}