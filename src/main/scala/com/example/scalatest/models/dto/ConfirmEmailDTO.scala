package com.example.scalatest.models.dto

case class ConfirmEmailDTO(token: String, password: String)
