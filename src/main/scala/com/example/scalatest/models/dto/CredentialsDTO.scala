package com.example.scalatest.models.dto

case class CredentialsDTO(email: String, password: String)
