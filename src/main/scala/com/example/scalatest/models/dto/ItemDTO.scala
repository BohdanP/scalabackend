package com.example.scalatest.models.dto

case class ItemDTO(id: String, name: String)
