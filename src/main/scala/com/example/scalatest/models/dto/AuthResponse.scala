package com.example.scalatest.models.dto

case class AuthResponse(
                         token: String,
                         userId: String,
                         email: String,
                       )
