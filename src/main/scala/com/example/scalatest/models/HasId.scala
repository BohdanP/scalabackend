package com.example.scalatest.models

trait HasId[ID] {
  val id: ID
}
