package com.example.scalatest
package services.main.impl

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import exceptions.Exceptions._
import models.dto.AuthResponse
import services.helpers.{BCryptHelper, JwtHelper}
import services.main.{EmailService, TokenService, UserService}
import helpers.CommonTest
import models.{Token, User}

import java.time.{Instant, ZoneOffset}
import org.scalatest.matchers.should.Matchers._

class AuthServiceImplTest extends CommonTest{
  val tokenServiceMock: TokenService = mock[TokenService]
  val emailServiceMock: EmailService = mock[EmailService]
  val userServiceMock: UserService = mock[UserService]
  val bcryptHelperMock: BCryptHelper = mock[BCryptHelper]
  val jwtHelperMock: JwtHelper = mock[JwtHelper]

  implicit val clock: java.time.Clock = java.time
    .Clock.fixed(Instant.parse("2022-01-21T09:22:01Z"), ZoneOffset.UTC)

  val authServiceImpl: AuthServiceImpl = new AuthServiceImpl(
    emailServiceMock,
    tokenServiceMock,
    bcryptHelperMock,
    userServiceMock,
    jwtHelperMock,
  )

  val user: User = User(
    id = "userid",
    email = "test@gmail.com",
    password = Some("123123"),
    createdAt = Instant.now(clock),
    updatedAt = Instant.now(clock),
    accountConfirmed = true,
  )

  "createPassword" should {
    val body = "token"
    val password = "123"
    val userId = "userId"
    val token = Token(
      id = "id",
      userId = userId,
      body = body,
      timestamp = Instant.now(clock)
    )
    "normal create password" in {
      tokenServiceMock.getByBody _ expects body returning IO.pure(token)
      tokenServiceMock.isTokenExpired _ expects token returning false
      userServiceMock.getById _ expects userId returning IO.pure(user)
      bcryptHelperMock.encrypt _ expects password returning user.password.get
      userServiceMock.update _ expects user.copy(
        password = user.password,
        updatedAt = Instant.now(clock),
        accountConfirmed = true
      ) returning IO.pure(true)

      authServiceImpl.createPassword(body, password)
        .unsafeRunSync() should be()
    }
    "token does not exists" in {
      tokenServiceMock.getByBody _ expects body returning IO.raiseError(NotFoundException("token"))

      an[InvalidTokenException.type] should be thrownBy authServiceImpl.createPassword(body, password).unsafeRunSync()
    }
    "token has expired" in {
      tokenServiceMock.getByBody _ expects body returning IO.pure(token)
      tokenServiceMock.isTokenExpired _ expects token returning true

      an[InvalidTokenException.type] should be thrownBy authServiceImpl.createPassword(body, password).unsafeRunSync()
    }
  }
  "createUser" should {
    val email = "email"
    val userId = "userId"
    val token = Token(
      id = "id",
      userId = userId,
      body = "body",
      timestamp = Instant.now(clock)
    )

    "normally create user" in {
      userServiceMock.createUser _ expects(email) returning IO.pure(userId)
      tokenServiceMock.createToken _ expects userId returning IO.pure(token)
      emailServiceMock.sendEmail _ expects(*, email, token.body) returning IO.unit

      authServiceImpl.createUser(email)
        .unsafeRunSync() should be()
    }
    "email is taken" in {
      userServiceMock.createUser _ expects(email) returning IO.raiseError(ForbiddenException(""))

      an[ForbiddenException] should be thrownBy authServiceImpl.createUser(email)
        .unsafeRunSync()
    }
  }
  "signIn" should {
    val plainPassword = "password"
    val token = "token"
    "normally sign in and return jwt with proper claims" in {
      userServiceMock.getByEmail _ expects user.email returning IO.pure(user)
      bcryptHelperMock.compare _ expects(plainPassword, user.password.get) returning true
      jwtHelperMock.encode _ expects(user, None) returning token

      authServiceImpl.signIn(user.email, plainPassword)
        .unsafeRunSync() shouldBe
        AuthResponse(
          token,
          user.id,
          user.email,
        )
    }
    "fails sign in if wrong email" in {
      userServiceMock.getByEmail _ expects user.email returning IO.raiseError(NotFoundException("User"))

      an[WrongCredentialsException.type] should be thrownBy (authServiceImpl.signIn(user.email, plainPassword).unsafeRunSync())
    }
    "fails sign in if wrong password" in {
      userServiceMock.getByEmail _ expects user.email returning IO.pure(user)
      bcryptHelperMock.compare _ expects(plainPassword, user.password.get) returning false

      an[WrongCredentialsException.type] should be thrownBy (authServiceImpl.signIn(user.email, plainPassword).unsafeRunSync())
    }
    "fails sign in if email not confirmed" in {
      userServiceMock.getByEmail _ expects user.email returning IO.pure(user.copy(accountConfirmed = false))

      an[EmailNotConfirmedException.type] should be thrownBy (authServiceImpl.signIn(user.email, plainPassword).unsafeRunSync())
    }
    }
  }
