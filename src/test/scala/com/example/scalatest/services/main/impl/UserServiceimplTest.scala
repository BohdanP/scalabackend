package com.example.scalatest
package services.main.impl

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import dao.UserDAO
import models.User
import org.scalamock.clazz.MockImpl.mock
import services.helpers.UUIDHelper
import org.scalatest.matchers.should.Matchers._
import exceptions.Exceptions._
import helpers.CommonTest

import java.time.{Instant, ZoneOffset}


class UserServiceimplTest extends CommonTest{
  val userDaoMock: UserDAO = mock[UserDAO]
  implicit val uuidHelperMock: UUIDHelper = mock[UUIDHelper]
  implicit val clock: java.time.Clock = java.time
    .Clock.fixed(Instant.parse("2022-01-21T09:22:01Z"), ZoneOffset.UTC)
  val userServiceImpl: UserServiceImpl = new UserServiceImpl(userDaoMock)

  val user: User = User(
    id = "userid",
    email = "user@gmail.com",
    password = "password",
    createdAt = Instant.now(clock),
    updatedAt = Instant.now(clock),
    accountConfirmed = true,
  )
  "createUserEmailNotTaken" should {
    "create user" in {
      userDaoMock.getByEmail _ expects user.email returning IO.pure(None)
      userDaoMock.createOne _ expects user returning IO.pure("")

      userServiceImpl.createUserEmailNotTaken(user, user.email)
        .unsafeRunSync() should be("")
    }

    "throw forbidden" in {
      userDaoMock.getByEmail _ expects user.email returning IO.pure(Some(user))

      an[ForbiddenException] should be thrownBy userServiceImpl.createUserEmailNotTaken(user,user.email)
        .unsafeRunSync()
    }
  }
}
