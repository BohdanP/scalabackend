package com.example.scalatest.helpers

import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.ExecutionContext
import org.scalamock.scalatest.MockFactory

trait CommonTest extends AnyWordSpec with MockFactory {
  implicit val ex: ExecutionContext = ExecutionContext.global
}
